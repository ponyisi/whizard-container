FROM centos:8
RUN yum -y install dnf-plugins-core \
    && yum -y --enablerepo=extras install epel-release \
    && yum -y config-manager --set-enabled powertools \
    && yum -y install wget gcc-c++ gcc-gfortran ocaml diffutils file libquadmath python2-devel make HepMC-devel libtirpc-devel pythia8-devel \
    && yum -y clean all \
    && wget --quiet http://whizard.hepforge.org/whizard-2.8.5.tar.gz \
    && mkdir whizard \
    && tar xzf whizard-2.8.5.tar.gz \
    && cd whizard-2.8.5 \
    && mkdir _build \
    && cd _build \
    && ../configure --prefix=`pwd`/../../whizard --enable-fc-openmp --enable-pythia8 \
    && make -j \
    && make -j install \
    && yum -y remove gcc-c++ gcc-gfortran ocaml-compiler-libs \
    && cd ../.. #\
    #&& rm -rf whizard-2.8.5
ENTRYPOINT ["/usr/bin/bash"]
CMD ["/usr/bin/bash"]
